<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="erro.jsp" %>

<c:if test="${empty lb}">
    <c:set var="msg" value="O usuario deve se autenticar para acessar o sistema!" scope="request"/>
    <jsp:forward page="index.jsp"/>
</c:if>

<!DOCTYPE html>
<html>
    
    <head>
        <title>Atendimento ao cliente</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <style>
            html, body {
                margin: 0px;
                padding: 0px;
                width: 100%;
                height: 100%;
                background-color: #e9ecef;
            }
        </style>
    </head>
    
    <body>
        
        <div class="jumbotron m-0">
            <div class="col-md-6 offset-md-3">

                <h1 class="display-4 mb-4 text-center">Atendimento ao cliente</h1>
                <form action="/Exercicios/AtendimentoServlet?action=new" method="POST">
                    
                    <h5 class="border-bottom border-dark">Informaçoes</h5>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="input-data">Data: </label>
                            <fmt:formatDate value="${data}" pattern="dd/MM/yyyy HH:mm:ss" var="dt"/>
                            <input type="text" name="data" value="${dt}" class="form-control" id="input-data" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="input-tipo">Tipo: </label>
                            <select class="custom-select" name="tipo" id="input-tipo">
                                <option selected>Selecione...</option>
                                <c:forEach items="${tipos}" var="tip">
                                    <option value="${tip.id}">${tip.nome}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="input-usuario">Usuario: </label>
                            <select class="custom-select" name="usuario" id="input-usuario">
                                <option selected>Selecione...</option>
                                <c:forEach items="${usuarios}" var="usu">
                                    <option value="${usu.id}">${usu.nome}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="input-cliente">Cliente: </label>
                            <select class="custom-select" name="cliente" id="input-cliente">
                                <option selected>Selecione...</option>
                                <c:forEach items="${clientes}" var="cli">
                                    <option value="${cli.id}">${cli.nome}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="input-produto">Produto: </label>
                            <select class="custom-select" name="produto" id="input-produto">
                                <option selected>Selecione...</option>
                                <c:forEach items="${produtos}" var="prd">
                                    <option value="${prd.id}">${prd.nome}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="input-descricao">Descriçao: </label>
                            <textarea name="descricao" class="form-control" id="input-descricao" 
                                      maxlength="255" rows="3" required></textarea>
                        </div>
                        <div class="form-check col-md-6 text-center">
                            <input class="form-check-input" type="radio" name="resolvido" id="input-radio-1" value="S" checked>
                            <label class="form-check-label" for="input-radio-1">Resolvido</label>
                        </div>
                        <div class="form-check col-md-6 text-center">
                            <input class="form-check-input" type="radio" name="resolvido" id="input-radio-1" value="N" checked>
                            <label class="form-check-label" for="input-radio-1">Não Resolvido</label>
                        </div>
                    </div>
                        
                    <a href="/Exercicios/portal.jsp" class="btn btn-dark float-left">Cancelar</a>
                    <button type="submit" class="btn btn-dark float-right">Salvar</button>
                    
                </form>

            </div>

        </div>
                 
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </body>
    
</html>