<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="erro.jsp" %>

<c:if test="${empty lb}">
    <c:set var="msg" value="O usuario deve se autenticar para acessar o sistema!" scope="request"/>
    <jsp:forward page="index.jsp"/>
</c:if>

<!DOCTYPE html>
<html>

    <head>
        <title>Portal</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <style>
            html, body {
                margin: 0px;
                padding: 0px;
                width: 100%;
                height: 100%;
                background-color: #e9ecef;
            }
        </style>
    </head>

    <body>

        <div class="jumbotron m-0">
            <div class="col-md-8 offset-md-2">

                <h1 class="display-4 mb-4 text-center">Portal</h1>

                <nav class="navbar navbar-expand-lg navbar-light bg-light border mb-2">
                    <a class="navbar-brand d-lg-none">Opções</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarToggler">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="/Exercicios/inserir.jsp">Inserir usuário</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/Exercicios/ClientesServlet">Clientes</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/Exercicios/AtendimentoServlet">Atendimentos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/Exercicios/relatorios.jsp">Relatórios</a>
                            </li>
                        </ul>
                        <div class="nav-item">
                            <span class="navbar-text mr-2">
                                Bem vindo, <b>${lb.nome}</b>
                            </span>                            
                        </div>
                        <a class="nav-link text-dark" href="/Exercicios/LogoutServlet">Sair</a>
                    </div>
                </nav>

                <table class="table table-hover text-center">
                    <thead class="bg-dark text-light">
                        <tr>
                            <th scope="col">Nome</th>
                            <th scope="col">Usuário</th>
                            <th scope="col">Senha</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${usuarios}" var="usu">
                            <tr>
                                <td>${usu.nome}</td>
                                <td>${usu.usuario}</td>
                                <td>${usu.senha}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

            </div>

        </div>

        <nav class="navbar navbar-light bg-light fixed-bottom justify-content-center">
            <span class="navbar-text">
                Em caso de problemas contactar o administrador: <b>${configuracao.email}</b>
            </span>
        </nav>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </body>

</html>