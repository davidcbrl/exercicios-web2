<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="erro.jsp" %>

<c:if test="${empty lb}">
    <c:set var="msg" value="O usuario deve se autenticar para acessar o sistema!" scope="request"/>
    <jsp:forward page="index.jsp"/>
</c:if>

<!DOCTYPE html>
<html>
    
    <head>
        <title>Inserir usuário</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <style>
            html, body {
                margin: 0px;
                padding: 0px;
                width: 100%;
                height: 100%;
                background-color: #e9ecef;
            }
        </style>
    </head>
    
    <body>
                       
        <div class="jumbotron m-0">

            <div class="col-md-6 offset-md-3">
                <h1 class="display-4 mb-4 text-center">Inserir usuário</h1>
                <form action="/Exercicios/CadastrarUsuarioServlet" method="POST">
                    
                    <div class="form-group">
                        <label for="input-nome">Nome: </label>
                        <input type="text" name="nome" class="form-control" id="input-nome" required/>
                    </div>
                    <div class="form-group">
                        <label for="input-usuario">Usuário: </label>
                        <input type="text" name="usuario" class="form-control" id="input-usuario" required/>
                    </div>
                    <div class="form-group">
                        <label for="input-senha">Senha: </label>
                        <input type="password" name="senha" class="form-control" id="input-senha" required/>
                    </div>
                    
                    <a href="/Exercicios/portal.jsp" class="btn btn-dark float-left">Cancelar</a>
                    <button type="submit" class="btn btn-dark float-right">Salvar</button>
                    
                </form>
            </div>

        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </body>
    
</html>