<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="erro.jsp" %>

<c:if test="${empty lb}">
    <c:set var="msg" value="O usuario deve se autenticar para acessar o sistema!" scope="request"/>
    <jsp:forward page="index.jsp"/>
</c:if>

<!DOCTYPE html>
<html>
    
    <head>
        <title>Clientes</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <style>
            html, body {
                margin: 0px;
                padding: 0px;
                width: 100%;
                height: 100%;
                background-color: #e9ecef;
            }
        </style>
    </head>
    
    <body>
        
        <div class="jumbotron m-0">
            <div class="col-md-10 offset-md-1">

                <h1 class="display-4 mb-4 text-center">Clientes</h1>
                
                <nav class="navbar navbar-expand-lg navbar-light bg-light border mb-2">
                    <a class="navbar-brand d-lg-none">Opções</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarToggler">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="/Exercicios/portal.jsp">Voltar</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/Exercicios/ClientesServlet?action=formNew">Novo cliente</a>
                            </li>
                        </ul>
                        <div class="nav-item">
                            <span class="navbar-text mr-2">
                                Logado como: <b>${lb.nome}</b>
                            </span>                            
                        </div>
                        <a class="nav-link text-dark" href="/Exercicios/LogoutServlet">Sair</a>
                    </div>
                </nav>

                <div class="table-responsive-md">
                    <table class="table table-hover text-center">
                        <thead class="bg-dark text-light">
                            <tr>
                                <th scope="col">CPF</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Email</th>
                                <th scope="col">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${clientes}" var="cli">
                                <tr>
                                    <td class="cpf">${cli.cpf}</td>
                                    <td>${cli.nome}</td>
                                    <td>${cli.email}</td>
                                    <td>
                                        <a href="/Exercicios/ClientesServlet?action=show&id=${cli.id}" class="btn btn-sm btn-dark" title="Visualizar"><i class="fa fa-list-ul"></i></a>
                                        <a href="/Exercicios/ClientesServlet?action=formUpdate&id=${cli.id}" class="btn btn-sm btn-dark" title="Alterar"><i class="fa fa-edit"></i></a>
                                        <button type="button" onclick="modalRemover(${cli.id})" class="btn btn-sm btn-dark" title="Remover"><i class="fa fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
                
        <nav class="navbar navbar-light bg-light fixed-bottom justify-content-center">
            <span class="navbar-text">
                Em caso de problemas contactar o administrador: <b>${configuracao.email}</b>
            </span>
        </nav>

        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="js/jquery.inputmask.bundle.js"></script>
        <script>
            $(document).ready(function(){
                $('.cpf').inputmask('999.999.999-99');
            });
        </script>
        <script>
            function modalRemover(id) {
                var r = confirm("Voce tem certeza que deseja remover o cliente?\nA acao nao pode ser revertida!");
                if (r == true) {
                    window.location = "/Exercicios/ClientesServlet?action=remove&id="+id;
                }
            }
        </script>
    </body>
    
</html>