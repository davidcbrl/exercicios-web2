<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="erro.jsp" %>

<c:if test="${empty lb}">
    <c:set var="msg" value="O usuario deve se autenticar para acessar o sistema!" scope="request"/>
    <jsp:forward page="index.jsp"/>
</c:if>

<!DOCTYPE html>
<html>
    
    <head>
        <title>Relatorios</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <style>
            html, body {
                margin: 0px;
                padding: 0px;
                width: 100%;
                height: 100%;
                background-color: #e9ecef;
            }
        </style>
    </head>
    
    <body>
        
        <div class="jumbotron m-0">
            <div class="col-md-8 offset-md-2">

                <h1 class="display-4 mb-4 text-center">Relatorios</h1>
                
                <nav class="navbar navbar-expand-lg navbar-light bg-light border mb-2">
                    <a class="navbar-brand d-lg-none">Opções</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarToggler">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="/Exercicios/portal.jsp">Voltar</a>
                            </li>
                        </ul>
                        <div class="nav-item">
                            <span class="navbar-text mr-2">
                                Logado como: <b>${lb.nome}</b>
                            </span>                            
                        </div>
                        <a class="nav-link text-dark" href="/Exercicios/LogoutServlet">Sair</a>
                    </div>
                </nav>
                            
                <div class="row m-0 p-0">
                    <a href="/Exercicios/GeradorRelatorio?action=relClientes" class="col-md-3 p-3 text-center text-dark shadow-sm border">
                        <h1><i class="fa fa-users"></i></h1>
                        <span>Todos os clientes</span>
                    </a>
                    <a href="/Exercicios/GeradorRelatorio?action=relResolvidos" class="col-md-3 p-3 text-center text-dark shadow-sm border">
                        <h1><i class="fa fa-clipboard-check"></i></h1>
                        <span>Todos os atendimentos resolvidos</span>
                    </a>
                    <a href="#" class="col-md-3 p-3 text-center text-dark shadow-sm border">
                        <h1><i class="fa fa-calendar-alt"></i></h1>
                        <span>Todos os atendimentos por data</span>
                    </a>
                    <div class="dropdown col-md-3 p-3 text-center text-dark shadow-sm border dropdown-toggle">
                        <button type="button" class="btn btn-transparent w-100 p-0" style="background-color:#e9ecef" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <h1><i class="fa fa-list-ul"></i></h1>
                            <p>Todos os atendimentos<br>por tipo</p>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="/Exercicios/GeradorRelatorio?action=atendimento-t1">Elogios</a>
                            <a class="dropdown-item" href="/Exercicios/GeradorRelatorio?action=atendimento-t2">Sugestões</a>
                            <a class="dropdown-item" href="/Exercicios/GeradorRelatorio?action=atendimento-t3">Dúvidas</a>
                            <a class="dropdown-item" href="/Exercicios/GeradorRelatorio?action=atendimento-t4">Reclamações</a>
                        </div>
                    </div>
                </div>
            
            </div>

        </div>
                
        <nav class="navbar navbar-light bg-light fixed-bottom justify-content-center">
            <span class="navbar-text">
                Em caso de problemas contactar o administrador: <b>${configuracao.email}</b>
            </span>
        </nav>
                
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </body>
    
</html>