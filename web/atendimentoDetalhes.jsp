<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="erro.jsp" %>

<c:if test="${empty lb}">
    <c:set var="msg" value="O usuario deve se autenticar para acessar o sistema!" scope="request"/>
    <jsp:forward page="index.jsp"/>
</c:if>

<!DOCTYPE html>
<html>
    
    <head>
        <title>Detalhes do atendimento</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <style>
            html, body {
                margin: 0px;
                padding: 0px;
                width: 100%;
                height: 100%;
                background-color: #e9ecef;
            }
        </style>
    </head>
    
    <body>
                       
        <div class="jumbotron m-0">

            <div class="col-md-6 offset-md-3">
                <h1 class="display-4 mb-4 text-center">Detalhes do atendimento</h1>

                <h5 class="border-bottom border-dark">Informaçoes</h5>
                <div class="row pb-3">
                    <fmt:formatDate value="${atendimento.data}" pattern="dd/MM/yyyy HH:mm:ss" var="dt"/>
                    <div class="col-md-6"><b>Data/Hora: </b>${dt}</div>
                    <div class="col-md-3"><b>Tipo: </b>${atendimento.tipo.nome}</div>
                    <div class="col-md-3"><b>Resolvido: </b>${atendimento.resolvido}</div>
                    <div class="col-md-12"><b>Descriçao: </b>${atendimento.descricao}</div>
                </div>
                <h5 class="border-bottom border-dark">Usuario</h5>
                <div class="row pb-3">
                    <div class="col-md-12"><b>Nome: </b>${atendimento.usuario.nome}</div>
                </div>
                <h5 class="border-bottom border-dark">Cliente</h5>
                <div class="row pb-3">
                    <div class="col-md-7"><b>Nome: </b>${atendimento.cliente.nome}</div>
                    <div class="col-md-5"><b>CPF: </b>${atendimento.cliente.cpf}</div>
                    <fmt:formatDate value="${atendimento.cliente.data}" pattern="dd/MM/yyyy" var="dt"/>
                    <div class="col-md-7"><b>Data de nascimento: </b>${dt}</div>
                    <div class="col-md-5"><b>Email: </b>${atendimento.cliente.email}</div>
                </div>
                <h5 class="border-bottom border-dark">Produto</h5>
                <div class="row pb-3">
                    <div class="col-md-12"><b>Nome: </b>${atendimento.produto.nome}</div>
                </div>

                <a href="/Exercicios/AtendimentoServlet" class="btn btn-dark float-left">Voltar</a>

            </div>

        </div>
                
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </body>
    
</html>