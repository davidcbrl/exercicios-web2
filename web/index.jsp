<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="erro.jsp" %>

<c:set var="sessao" value="${request.getSession(false)}"/>
<c:if test="${!empty sessao}">
    <c:remove var="sessao" scope="session"/>
</c:if>

<!DOCTYPE html>
<html>
    
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <style>
            html, body {
                margin: 0px;
                padding: 0px;
                width: 100%;
                height: 100%;
                background-color: #e9ecef;
            }
        </style>
    </head>
    
    <body>
                       
        <div class="jumbotron m-0">

            <div class="col-md-4 offset-md-4">
                <h1 class="display-1 mb-5 text-center">Login</h1>
                <form action="/Exercicios/LoginServlet" method="POST">
                    
                    <div class="form-group">
                        <c:if test="${!empty msg}">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                ${msg}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </div>
                        </c:if>
                        <label for="input-usuario">Usuário: </label>
                        <input type="text" name="usuario" class="form-control mb-2" id="input-usuario" required/>
                        <label for="input-senha">Senha: </label>
                        <input type="password" name="senha" class="form-control mb-2" id="input-senha" required/>
                    </div>
                        
                    <button type="submit" class="btn btn-dark col-12 mt-2">Login</button>
                    
                </form>
            </div>

        </div>
        
        <nav class="navbar navbar-light bg-light fixed-bottom justify-content-center">
            <span class="navbar-text">
                Em caso de problemas contactar o administrador: <b>${configuracao.email}</b>
            </span>
        </nav>
        
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </body>
    
</html>