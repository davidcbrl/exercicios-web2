<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page isErrorPage="true" %>

<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <title>Erro</title>
        <style>
            html, body {
                margin: 0px;
                padding: 0px;
                width: 100%;
                height: 100%;
                background-color: #e9ecef;
            }
        </style>
    </head>
    
    <body>
        
        <div class="jumbotron h-100 m-0">
            <div class="row h-100">
                
                <div class="col text-center align-self-center">
                    <h1>Exceçao:</h1>
                    <p>${pageContext.exception.message}</p>
                    <h1>Stack Trace:</h1>
                    <p>${pageContext.out.flush()}</p>
                    <%
                        String msg = (String)request.getAttribute("msg");
                        String link = (String)request.getAttribute("page");
                        if(msg != null && link != null) {
                            out.println("<h1 class='text-danger'>" + msg + " <a href='/Exercicios" + link + "' class='text-dark'>Voltar</a></h1>");
                        } else {
                            out.println("<h1 class='text-danger'><a href='/Exercicios/portal.jsp' class='text-dark'>Voltar</a></h1>");
                        }
                    %>
                </div>
                
            </div>
        </div>
        
        <nav class="navbar navbar-light bg-light fixed-bottom justify-content-center">
            <span class="navbar-text">
                Em caso de problemas contactar o administrador: <b>${configuracao.email}</b>
            </span>
        </nav>
        
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </body>
</html>