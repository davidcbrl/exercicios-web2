<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="erro.jsp" %>

<c:if test="${empty lb}">
    <c:set var="msg" value="O usuario deve se autenticar para acessar o sistema!" scope="request"/>
    <jsp:forward page="index.jsp"/>
</c:if>

<!DOCTYPE html>
<html>
    
    <head>
        <title>Visualizar cliente</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <style>
            html, body {
                margin: 0px;
                padding: 0px;
                width: 100%;
                height: 100%;
                background-color: #e9ecef;
            }
        </style>
    </head>
    
    <body>
                       
        <div class="jumbotron m-0">

            <div class="col-md-6 offset-md-3">
                <h1 class="display-4 mb-4 text-center">Visualizar cliente</h1>
                    
                <h5 class="border-bottom border-dark">Dados pessoais</h5>
                <div class="row pb-3">
                    <div class="col-md-5"><b>CPF: </b><span class="cpf">${cliente.cpf}</span></div>
                    <div class="col-md-7"><b>Nome: </b>${cliente.nome}</div>
                    <div class="col-md-5"><b>Email: </b>${cliente.email}</div>
                    <fmt:formatDate value="${cliente.data}" pattern="dd/MM/yyyy" var="dt"/>
                    <div class="col-md-7"><b>Data de nascimento: </b>${dt}</div>
                </div>
                <h5 class="border-bottom border-dark">Endereço</h5>
                <div class="row pb-3">
                    <div class="col-md-5"><b>Rua: </b>${cliente.rua}</div>
                    <div class="col-md-3"><b>Numero: </b>${cliente.numero}</div>
                    <div class="col-md-4"><b>CEP: </b><span class="cep">${cliente.cep}</span></div>
                    <div class="col-md-5"><b>Cidade: </b>${cliente.cidade.nome}</div>
                    <div class="col-md-7"><b>Estado: </b>${cliente.cidade.estado.nome}</div>
                </div>

                <a href="/Exercicios/ClientesServlet" class="btn btn-dark float-left">Voltar</a>
                <button type="button" class="btn btn-dark float-right ml-2" data-toggle="modal" data-target="#modalRemover" title="Remover"><i class="fa fa-trash-alt"></i></button>
                <a href="/Exercicios/ClientesServlet?action=formUpdate&id=${cliente.id}" class="btn btn-dark float-right ml-2" title="Alterar"><i class="fa fa-edit"></i></a>

            </div>

        </div>
                
        <div class="modal fade" id="modalRemover" tabindex="-1" role="dialog" aria-labelledby="modalRemoverTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalRemoverTitle"><i class="fa fa-trash-alt"></i> Remover cliente</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Voce tem certeza que deseja remover o cliente? A açao nao pode ser revertida!
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">Nao</button>
                        <a href="/Exercicios/ClientesServlet?action=remove&id=${cliente.id}" class="btn btn-dark">Sim</a>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="js/jquery.inputmask.bundle.js"></script>
        <script>
            $(document).ready(function(){
                $('.cpf').inputmask('999.999.999-99');
                $('.cep').inputmask('99999-999');
            });
        </script>
    </body>
    
</html>
