<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="erro.jsp" %>

<c:if test="${empty lb}">
    <c:set var="msg" value="O usuario deve se autenticar para acessar o sistema!" scope="request"/>
    <jsp:forward page="index.jsp"/>
</c:if>

<!DOCTYPE html>
<html>

    <head>
        <c:choose>
            <c:when test="${cliente != null}">
                <title>Alterar cliente</title>
            </c:when>
            <c:otherwise>
                <title>Novo cliente</title>
            </c:otherwise>
        </c:choose>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <style>
            html, body {
                margin: 0px;
                padding: 0px;
                width: 100%;
                height: 100%;
                background-color: #e9ecef;
            }
        </style>
    </head>

    <body>

        <div class="jumbotron m-0">

            <div class="col-md-6 offset-md-3">
                <c:choose>
                    <c:when test="${cliente != null}">
                        <h1 class="display-4 mb-4 text-center">Alterar cliente</h1>
                        <form action="/Exercicios/ClientesServlet?action=update" method="POST">
                    </c:when>
                    <c:otherwise>
                        <h1 class="display-4 mb-4 text-center">Novo cliente</h1>
                        <form action="/Exercicios/ClientesServlet?action=new" method="POST">
                        </c:otherwise>
                    </c:choose>


                    <h5 class="border-bottom border-dark">Dados pessoais</h5>
                    <div class="form-row">
                        <input type="hidden" name="id" value="${cliente.id}">
                        <div class="form-group col-md-4">
                            <label for="input-cpf">CPF: </label>
                            <input type="text" name="cpf" value="${cliente.cpf}" class="form-control" id="input-cpf"
                                   id="input-cpf" placeholder="000.000.000-00" required/>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="input-nome">Nome: </label>
                            <input type="text" name="nome" value="${cliente.nome}" class="form-control"
                                   id="input-nome" maxlength="100" placeholder="Exemplo" required/>
                        </div>
                        <div class="form-group col-md-7">
                            <label for="input-email">Email: </label>
                            <input type="email" name="email" value="${cliente.email}" class="form-control"
                                   id="input-email" maxlength="100" placeholder="exemplo@exemplo" required/>
                        </div>
                        <div class="form-group col-md-5">
                            <label for="input-data">Data de nascimento: </label>
                            <input type="date" name="data" value="${cliente.data}" class="form-control"
                                   id="input-data" placeholder="dd/mm/aaaa" required/>

                        </div>
                    </div>
                    <h5 class="border-bottom border-dark">Endereço</h5>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="input-rua">Rua: </label>
                            <input type="text" name="rua" value="${cliente.rua}" class="form-control"
                                   id="input-rua" maxlength="100" placeholder="Exemplo" required/>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="input-numero">Numero: </label>
                            <input type="text" name="numero" value="${cliente.numero}" class="form-control"
                                   id="input-numero" placeholder="00000" required/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="input-cep">CEP: </label>
                            <input type="text" name="cep" value="${cliente.cep}" class="form-control"
                                   id="input-cep" placeholder="00000-000" required/>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="input-cidade">Estado: </label>
                            <select class="custom-select" name="estado" id="estado" required>
                                <option selected>${!empty cliente.cidade.estado.nome ? cliente.cidade.estado.nome : 'Selecione...'}</option>
                                <c:forEach items="${estados}" var="est">
                                    <option value="${est.id}">${est.nome}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="input-cidade">Cidade: </label>
                            <select class="custom-select" name="cidade" id="cidade" required>
                                <!-- cidades -->
                            </select>
                        </div>
                    </div>

                    <a href="/Exercicios/ClientesServlet" class="btn btn-dark float-left">Cancelar</a>
                    <button type="submit" class="btn btn-dark float-right">Salvar</button>

                </form>
            </div>

        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="js/jquery.inputmask.bundle.js"></script>
        <script>
            $(document).ready(function () {
                $("#input-cpf").inputmask("999.999.999-99");
                $("#input-data").inputmask("0-3{1}0-9{1}/0-1{1}0-9{1}/0-9{4}");
                $("#input-numero").inputmask("9{1,5}");
                $("#input-cep").inputmask("99999-999");
            });
        </script>
        <script>
            $("#input-cpf").on("change", function() {
                var v = $("#input-cpf").val();
                v = v.replace(/[.-]/g, '');
                if(!validaCPF(v)) {
                    alert("CPF inválido!");
                }
            });
            function validaCPF(strCPF) {
                var Soma;
                var Resto;
                Soma = 0;
                if (strCPF.length != 11 ||
                  strCPF == "00000000000" ||
                  strCPF == "11111111111" ||
                  strCPF == "22222222222" ||
                  strCPF == "33333333333" ||
                  strCPF == "44444444444" ||
                  strCPF == "55555555555" ||
                  strCPF == "66666666666" ||
                  strCPF == "77777777777" ||
                  strCPF == "88888888888" ||
                  strCPF == "99999999999") {
                  return false;
                }
                if (strCPF == "00000000000") return false;
                for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
                Resto = (Soma * 10) % 11;
                if ((Resto == 10) || (Resto == 11))  Resto = 0;
                if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;
                Soma = 0;
                for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
                Resto = (Soma * 10) % 11;
                if ((Resto == 10) || (Resto == 11))  Resto = 0;
                if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
                return true;
            }
        </script>
        <script>
            $(document).ready(function () {
                $("#estado").change(function () {
                    getCidades();
                });
            });
            function getCidades() {
                var estadoId = $("#estado").val();
                var url = "AJAXServlet";
                $.ajax({
                    url: url,
                    data: {
                        estadoId: estadoId
                    },
                    dataType: 'json',
                    success: function (data) {
                        $("#cidade").empty();
                        $("#cidade").append('<option selected>Selecione...</option>');
                        $.each(data, function (i, obj) {
                            $("#cidade").append('<option value=' + obj.id + '>' + obj.nome + '</option>');
                        });
                    },
                    error: function (request, textStatus, errorThrown) {
                        alert(request.status + ', Error: ' + request.statusText);
                    }
                });
            }
        </script>
    </body>

</html>
