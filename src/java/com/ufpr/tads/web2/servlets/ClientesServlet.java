package com.ufpr.tads.web2.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.ufpr.tads.web2.beans.ClienteBean;
import com.ufpr.tads.web2.beans.CidadeBean;
import com.ufpr.tads.web2.facade.ClientesFacade;
import com.ufpr.tads.web2.exceptions.UsuarioNaoAutenticadoException;

@WebServlet(name = "ClientesServlet", urlPatterns = {"/ClientesServlet"})
public class ClientesServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        try {
            HttpSession s = request.getSession(false);
            if (s == null || s.getAttribute("lb") == null) {
                throw new UsuarioNaoAutenticadoException();
            }
        } catch (UsuarioNaoAutenticadoException e) {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");
            request.setAttribute("msg", "O usuário deve se autenticar para acessar o sistema!");
            rd.forward(request, response);
        }
        
        String action = request.getParameter("action");
        if (action == null || "list".equals(action)) {
            request.setAttribute("clientes", ClientesFacade.buscarTodos());
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/clientesListar.jsp");
            rd.forward(request, response);
        }
        if ("show".equals(action)) {
            int idCliente = Integer.parseInt(request.getParameter("id"));
            request.setAttribute("cliente", ClientesFacade.buscar(idCliente));
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/clientesVisualizar.jsp");
            rd.forward(request, response);
        }
        if ("formNew".equals(action)) {
            request.setAttribute("estados", ClientesFacade.buscarEstados());
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/clientesForm.jsp");
            rd.forward(request, response);
        }
        if ("new".equals(action)) {
            ClienteBean cli = new ClienteBean();
            String cpf = request.getParameter("cpf");
            String cpfOK = cpf.replaceAll("[.-]","");
            cli.setCpf(cpfOK);
            cli.setNome(request.getParameter("nome"));
            cli.setEmail(request.getParameter("email"));
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String str = request.getParameter("data");
            Date data = null;
            try {
                data = format.parse(str);
            } catch (ParseException e) {
                System.out.println("Erro ao converter data para cadastro do cliente! :(");
            }
            cli.setData(data);
            cli.setRua(request.getParameter("rua"));
            cli.setNumero(Integer.parseInt(request.getParameter("numero")));
            String cep = request.getParameter("cep");
            String cepOK = cep.replaceAll("[.-]","");
            cli.setCep(cepOK);
            int idCidade = Integer.parseInt(request.getParameter("cidade"));
            CidadeBean cb = ClientesFacade.buscarCidadeCliente(idCidade);
            cli.setCidade(cb);
            ClientesFacade.inserir(cli);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/ClientesServlet?action=list");
            rd.forward(request, response);
        }
        if ("formUpdate".equals(action)) {
            int idCliente = Integer.parseInt(request.getParameter("id"));
            request.setAttribute("cliente", ClientesFacade.buscar(idCliente));
            request.setAttribute("estados", ClientesFacade.buscarEstados());
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/clientesForm.jsp");
            rd.forward(request, response);
        }
        if ("update".equals(action)) {
            ClienteBean cli = new ClienteBean();
            cli.setId(Integer.parseInt(request.getParameter("id")));
            String cpf = request.getParameter("cpf");
            String cpfOK = cpf.replaceAll("[.-]","");
            cli.setCpf(cpfOK);
            cli.setNome(request.getParameter("nome"));
            cli.setEmail(request.getParameter("email"));
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String str = request.getParameter("data");
            Date data = null;
            try {
                data = format.parse(str);
            } catch (ParseException e) {
                System.out.println("Erro ao converter data para cadastro do cliente! :(");
            }
            cli.setData(data);
            cli.setRua(request.getParameter("rua"));
            cli.setNumero(Integer.parseInt(request.getParameter("numero")));
            String cep = request.getParameter("cep");
            String cepOK = cep.replaceAll("[.-]","");
            cli.setCep(cepOK);
            int idCidade = Integer.parseInt(request.getParameter("cidade"));
            CidadeBean cb = ClientesFacade.buscarCidadeCliente(idCidade);
            cli.setCidade(cb);
            ClientesFacade.alterar(cli);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/ClientesServlet?action=list");
            rd.forward(request, response);
        }
        if ("remove".equals(action)) {
            int idCliente = Integer.parseInt(request.getParameter("id"));
            ClientesFacade.remover(idCliente);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/ClientesServlet?action=list");
            rd.forward(request, response);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
