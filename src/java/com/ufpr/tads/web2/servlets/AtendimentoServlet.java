package com.ufpr.tads.web2.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.ufpr.tads.web2.facade.AtendimentoFacade;
import com.ufpr.tads.web2.beans.AtendimentoBean;
import com.ufpr.tads.web2.beans.TipoAtendimentoBean;
import com.ufpr.tads.web2.beans.UsuarioBean;
import com.ufpr.tads.web2.beans.ClienteBean;
import com.ufpr.tads.web2.beans.ProdutoBean;
import com.ufpr.tads.web2.exceptions.UsuarioNaoAutenticadoException;

@WebServlet(name = "AtendimentoServlet", urlPatterns = {"/AtendimentoServlet"})
public class AtendimentoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        try {
            HttpSession s = request.getSession(false);
            if (s == null || s.getAttribute("lb") == null) {
                throw new UsuarioNaoAutenticadoException();
            }
        } catch (UsuarioNaoAutenticadoException e) {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");
            request.setAttribute("msg", "O usuário deve se autenticar para acessar o sistema!");
            rd.forward(request, response);
        }
        
        String action = request.getParameter("action");
        if (action == null || "list".equals(action)) {
            request.setAttribute("atendimentos", AtendimentoFacade.buscarTodos());
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/atendimentoListar.jsp");
            rd.forward(request, response);
        }
        if ("formNew".equals(action)) {
            request.setAttribute("data", new Date());
            request.setAttribute("tipos", AtendimentoFacade.buscarTiposAtendimento());
            request.setAttribute("usuarios", AtendimentoFacade.buscarUsuarios());
            request.setAttribute("clientes", AtendimentoFacade.buscarClientes());
            request.setAttribute("produtos", AtendimentoFacade.buscarProdutos());
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/atendimento.jsp");
            rd.forward(request, response);
        }
        if ("new".equals(action)) {
            AtendimentoBean atd = new AtendimentoBean();
            atd.setDescricao(request.getParameter("descricao"));
            atd.setResolvido(request.getParameter("resolvido"));

            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String str = request.getParameter("data");
            Date data = null;
            try {
                data = format.parse(str);
            } catch (ParseException e) {
                System.out.println("Erro ao converter data para atendimento! :(");
            }
            atd.setData(data);
            int idAux;
            idAux = Integer.parseInt(request.getParameter("tipo"));
            TipoAtendimentoBean tb = AtendimentoFacade.buscarTipo(idAux);
            atd.setTipo(tb);
            idAux = Integer.parseInt(request.getParameter("usuario"));
            UsuarioBean ub = AtendimentoFacade.buscarUsuario(idAux);
            atd.setUsuario(ub);
            idAux = Integer.parseInt(request.getParameter("cliente"));
            ClienteBean cb = AtendimentoFacade.buscarCliente(idAux);
            atd.setCliente(cb);
            idAux = Integer.parseInt(request.getParameter("produto"));
            ProdutoBean pb = AtendimentoFacade.buscarProduto(idAux);
            atd.setProduto(pb);
            AtendimentoFacade.inserir(atd);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/AtendimentoServlet?action=list");
            rd.forward(request, response);
        }
        if ("show".equals(action)) {
            int idAtendimento = Integer.parseInt(request.getParameter("id"));
            request.setAttribute("atendimento", AtendimentoFacade.buscar(idAtendimento));
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/atendimentoDetalhes.jsp");
            rd.forward(request, response);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
