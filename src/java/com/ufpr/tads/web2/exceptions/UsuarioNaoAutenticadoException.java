package com.ufpr.tads.web2.exceptions;

public class UsuarioNaoAutenticadoException extends AppException {

    public UsuarioNaoAutenticadoException() {
    }

    public UsuarioNaoAutenticadoException(String msg) {
        super(msg);
    }

}
