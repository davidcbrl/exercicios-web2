package com.ufpr.tads.web2.exceptions;

public class UsuarioSenhaInvalidosException extends AppException {

    public UsuarioSenhaInvalidosException() {
    }

    public UsuarioSenhaInvalidosException(String msg) {
        super(msg);
    }
}
