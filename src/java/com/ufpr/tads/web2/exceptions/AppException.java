package com.ufpr.tads.web2.exceptions;

public class AppException extends Exception {

    public AppException() {
    }

    public AppException(String msg) {
        super(msg);
    }

}
