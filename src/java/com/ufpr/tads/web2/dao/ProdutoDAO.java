package com.ufpr.tads.web2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.ufpr.tads.web2.beans.ProdutoBean;

public class ProdutoDAO {
    
    /**
     * Busca todos os produtos cadastrados na base de dados.
     * @return A `lista` de produtos cadastrados.
     */
    public static List<ProdutoBean> buscarTodos() {
        List<ProdutoBean> resultados = new ArrayList<ProdutoBean>();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_produto;");
            rs = st.executeQuery();
            while (rs.next()) {
                ProdutoBean prd = new ProdutoBean();
                prd.setId(rs.getInt("id_produto"));
                prd.setNome(rs.getString("nome_produto"));
                resultados.add(prd);
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter produtos! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return resultados;
    }
    
    /**
     * Busca um produto cadastrado na base de dados com o `id` especificado.
     * @param idProduto
     * @return O `objeto` de produto se existir, senão retorna `null`.
     */
    public static ProdutoBean buscarPorId(int idProduto) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_produto WHERE id_produto = ?;");
            st.setInt(1, idProduto);
            rs = st.executeQuery();
            if (rs.next()) {
                ProdutoBean prd = new ProdutoBean();
                prd.setId(rs.getInt("id_produto"));
                prd.setNome(rs.getString("nome_produto"));
                return prd;
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter dados do produto! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return null;
    }
    
}
