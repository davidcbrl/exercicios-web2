package com.ufpr.tads.web2.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionFactory {

    /**
     * Fornece a conexão JDBC com a base de dados.
     * @return A conexão aberta.
     */
    public static Connection getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://localhost:3306/javaee", "root", "admin");
        } catch(Exception e) {
            System.out.println("Erro ao estabelecer conexão! :(");
        }
        return null;
    }

}