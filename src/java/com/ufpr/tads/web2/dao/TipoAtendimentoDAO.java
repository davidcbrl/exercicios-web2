package com.ufpr.tads.web2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.ufpr.tads.web2.beans.TipoAtendimentoBean;

public class TipoAtendimentoDAO {
    
    /**
     * Busca todos os tipos de atendimento cadastrados na base de dados.
     * @return A `lista` de tipos de atendimento cadastrados.
     */
    public static List<TipoAtendimentoBean> buscarTodos() {
        List<TipoAtendimentoBean> resultados = new ArrayList<TipoAtendimentoBean>();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_tipo_atendimento;");
            rs = st.executeQuery();
            while (rs.next()) {
                TipoAtendimentoBean tpa = new TipoAtendimentoBean();
                tpa.setId(rs.getInt("id_tipo_atendimento"));
                tpa.setNome(rs.getString("nome_tipo_atendimento"));
                resultados.add(tpa);
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter tipos de atendimento! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return resultados;
    }
    
    /**
     * Busca um tipo de atendimento cadastrado na base de dados com o `id` especificado.
     * @param idTipoAtendimento
     * @return O `objeto` de tipo de atendimento se existir, senão retorna `null`.
     */
    public static TipoAtendimentoBean buscarPorId(int idTipoAtendimento) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_tipo_atendimento WHERE id_tipo_atendimento = ?;");
            st.setInt(1, idTipoAtendimento);
            rs = st.executeQuery();
            if (rs.next()) {
                TipoAtendimentoBean tpa = new TipoAtendimentoBean();
                tpa.setId(rs.getInt("id_tipo_atendimento"));
                tpa.setNome(rs.getString("nome_tipo_atendimento"));
                return tpa;
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter dados do tipo de atendimento! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return null;
    }
    
}
