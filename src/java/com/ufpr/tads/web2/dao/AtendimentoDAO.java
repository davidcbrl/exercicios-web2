package com.ufpr.tads.web2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.ufpr.tads.web2.beans.AtendimentoBean;
import com.ufpr.tads.web2.beans.TipoAtendimentoBean;
import com.ufpr.tads.web2.beans.UsuarioBean;
import com.ufpr.tads.web2.beans.ClienteBean;
import com.ufpr.tads.web2.beans.ProdutoBean;

public class AtendimentoDAO {
    
    /**
     * Insere um atendimento na base de dados.
     * @param atendimento 
     */
    public static void inserir(AtendimentoBean atendimento) {
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("INSERT INTO tb_atendimento (dsc_atendimento, dt_hr_atendimento, res_atendimento, id_tipo_atendimento, id_usuario, id_cliente, id_produto) VALUES (?, ?, ?, ?, ?, ?, ?);");
            st.setString(1, atendimento.getDescricao());
            java.util.Date dt = atendimento.getData();
            java.sql.Timestamp data = new java.sql.Timestamp(dt.getTime());
            st.setTimestamp(2, data);
            st.setString(3, atendimento.getResolvido());
            st.setInt(4, atendimento.getTipo().getId());
            st.setInt(5, atendimento.getUsuario().getId());
            st.setInt(6, atendimento.getCliente().getId());
            st.setInt(7, atendimento.getProduto().getId());
            st.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
    }
    
    /**
     * Busca todos os atendimentos cadastrados na base de dados.
     * @return A `lista` de atendimentos cadastrados.
     */
    public static List<AtendimentoBean> buscarTodos() {
        List<AtendimentoBean> resultados = new ArrayList<AtendimentoBean>();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_atendimento;");
            rs = st.executeQuery();
            while (rs.next()) {
                AtendimentoBean atd = new AtendimentoBean();
                atd.setId(rs.getInt("id_atendimento"));
                atd.setData(rs.getTimestamp("dt_hr_atendimento"));
                UsuarioBean usu = UsuarioDAO.buscarPorId(rs.getInt("id_usuario"));
                atd.setUsuario(usu);
                ClienteBean cli = ClienteDAO.buscarPorId(rs.getInt("id_cliente"));
                atd.setCliente(cli);
                ProdutoBean prd = ProdutoDAO.buscarPorId(rs.getInt("id_produto"));
                atd.setProduto(prd);
                resultados.add(atd);
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter atendimentos! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return resultados;
    }
    
    /**
     * Busca um atendimento cadastrado na base de dados com o `id` especificado.
     * @param idAtendimento
     * @return O `objeto` de atendimento se existir, senão retorna `null`.
     */
    public static AtendimentoBean buscarPorId(int idAtendimento) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT at.*, ta.*, us.*, cl.*, pr.* FROM tb_atendimento at, tb_tipo_atendimento ta, tb_usuario us, tb_cliente cl, tb_produto pr WHERE id_atendimento = ? AND ta.id_tipo_atendimento = at.id_tipo_atendimento AND us.id_usuario = at.id_usuario AND cl.id_cliente = at.id_cliente AND pr.id_produto = at.id_produto;");
            st.setInt(1, idAtendimento);
            rs = st.executeQuery();
            if (rs.next()) {
                AtendimentoBean atd = new AtendimentoBean();
                atd.setId(rs.getInt("id_atendimento"));
                atd.setDescricao(rs.getString("dsc_atendimento"));
                atd.setData(rs.getTimestamp("dt_hr_atendimento"));
                atd.setResolvido(rs.getString("res_atendimento"));
                TipoAtendimentoBean tpa = new TipoAtendimentoBean();
                tpa.setId(rs.getInt("id_tipo_atendimento"));
                tpa.setNome(rs.getString("nome_tipo_atendimento"));
                UsuarioBean usu = new UsuarioBean();
                usu.setId(rs.getInt("id_usuario"));
                usu.setNome(rs.getString("nome_usuario"));
                ClienteBean cli = new ClienteBean();
                cli.setId(rs.getInt("id_cliente"));
                cli.setNome(rs.getString("nome_cliente"));
                cli.setCpf(rs.getString("cpf_cliente"));
                cli.setData(rs.getDate("data_cliente"));
                cli.setEmail(rs.getString("email_cliente"));
                ProdutoBean prd = new ProdutoBean();
                prd.setId(rs.getInt("id_produto"));
                prd.setNome(rs.getString("nome_produto"));
                atd.setTipo(tpa);
                atd.setUsuario(usu);
                atd.setCliente(cli);
                atd.setProduto(prd);
                return atd;
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter dados do atendimento! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return null;
    }

}
