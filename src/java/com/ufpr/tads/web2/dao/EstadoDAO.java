package com.ufpr.tads.web2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.ufpr.tads.web2.beans.EstadoBean;

public class EstadoDAO {
    
    /**
     * Busca todos os estados cadastrados na base de dados.
     * @return A `lista` de estados cadastrados.
     */
    public static List<EstadoBean> buscarTodos() {
        List<EstadoBean> resultados = new ArrayList<EstadoBean>();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_estado;");
            rs = st.executeQuery();
            while (rs.next()) {
                EstadoBean est = new EstadoBean();
                est.setId(rs.getInt("id_estado"));
                est.setNome(rs.getString("nome_estado"));
                est.setSigla(rs.getString("sigla_estado"));
                resultados.add(est);
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter estados! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return resultados;
    }
    
    /**
     * Busca um estado cadastrado na base de dados com o `id` especificado.
     * @param idEstado
     * @return O `objeto` de estado se existir, senão retorna `null`.
     */
    public static EstadoBean buscarPorId(int idEstado) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_estado WHERE id_estado = ?;");
            st.setInt(1, idEstado);
            rs = st.executeQuery();
            if (rs.next()) {
                EstadoBean est = new EstadoBean();
                est.setId(rs.getInt("id_estado"));
                est.setNome(rs.getString("nome_estado"));
                est.setSigla(rs.getString("sigla_estado"));
                return est;
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter dados do estado! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return null;
    }
    
}