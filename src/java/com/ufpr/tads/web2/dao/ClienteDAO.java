package com.ufpr.tads.web2.dao;

import com.ufpr.tads.web2.beans.CidadeBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.ufpr.tads.web2.beans.ClienteBean;
import com.ufpr.tads.web2.beans.EstadoBean;

public class ClienteDAO {
        
    /**
     * Insere um cliente na base de dados.
     * @param cliente 
     */
    public static void inserir(ClienteBean cliente) {
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("INSERT INTO tb_cliente (cpf_cliente, nome_cliente, email_cliente, data_cliente, rua_cliente, nr_cliente, cep_cliente, id_cidade) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
            st.setString(1, cliente.getCpf());
            st.setString(2, cliente.getNome());
            st.setString(3, cliente.getEmail());
            java.util.Date dt = cliente.getData();
            java.sql.Date data = new java.sql.Date(dt.getTime());
            st.setDate(4, data);
            st.setString(5, cliente.getRua());
            st.setInt(6, cliente.getNumero());
            st.setString(7, cliente.getCep());
            st.setInt(8, cliente.getCidade().getId());
            st.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
    }
    
    /**
     * Busca todos os clientes cadastrados na base de dados.
     * @return A `lista` de clientes cadastrados.
     */
    public static List<ClienteBean> buscarTodos() {
        List<ClienteBean> resultados = new ArrayList<ClienteBean>();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_cliente;");
            rs = st.executeQuery();
            while (rs.next()) {
                ClienteBean cli = new ClienteBean();
                cli.setId(rs.getInt("id_cliente"));
                cli.setCpf(rs.getString("cpf_cliente"));
                cli.setNome(rs.getString("nome_cliente"));
                cli.setEmail(rs.getString("email_cliente"));
                resultados.add(cli);
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter clientes! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return resultados;
    }
    
    /**
     * Busca um cliente cadastrado na base de dados com o `id` especificado.
     * @param idCliente
     * @return O `objeto` de cliente se existir, senão retorna `null`.
     */
    public static ClienteBean buscarPorId(int idCliente) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT cl.*, ci.*, es.* FROM tb_cidade ci, tb_estado es, tb_cliente cl WHERE id_cliente = ? AND cl.id_cidade = ci.id_cidade AND es.id_estado = ci.id_estado;");
            st.setInt(1, idCliente);
            rs = st.executeQuery();
            if (rs.next()) {
                ClienteBean cli = new ClienteBean();
                cli.setId(rs.getInt("id_cliente"));
                cli.setCpf(rs.getString("cpf_cliente"));
                cli.setNome(rs.getString("nome_cliente"));
                cli.setEmail(rs.getString("email_cliente"));
                cli.setData(rs.getDate("data_cliente"));
                cli.setRua(rs.getString("rua_cliente"));
                cli.setNumero(rs.getInt("nr_cliente"));
                cli.setCep(rs.getString("cep_cliente"));
                CidadeBean cid = new CidadeBean();
                cid.setId(rs.getInt("id_cidade"));
                cid.setNome(rs.getString("nome_cidade"));
                EstadoBean est = new EstadoBean();
                est.setId(rs.getInt("id_estado"));
                est.setNome(rs.getString("nome_estado"));
                est.setSigla(rs.getString("sigla_estado"));
                cid.setEstado(est);
                cli.setCidade(cid);
                return cli;
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter dados do cliente! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return null;
    }
    
    /**
     * Altera um cliente na base de dados.
     * @param cliente 
     */
    public static void alterar(ClienteBean cliente) {
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("UPDATE tb_cliente SET cpf_cliente = ?, nome_cliente = ?, email_cliente = ?, data_cliente = ?, rua_cliente = ?, nr_cliente = ?, cep_cliente = ?, id_cidade = ? where id_cliente = ?;");
            st.setString(1, cliente.getCpf());
            st.setString(2, cliente.getNome());
            st.setString(3, cliente.getEmail());
            java.util.Date dt = cliente.getData();
            java.sql.Date data = new java.sql.Date(dt.getTime());
            st.setDate(4, data);
            st.setString(5, cliente.getRua());
            st.setInt(6, cliente.getNumero());
            st.setString(7, cliente.getCep());
            st.setInt(8, cliente.getCidade().getId());
            st.setInt(9, cliente.getId());
            st.executeUpdate();
        } catch(Exception e) {
            System.out.println("Erro ao alterar dados do cliente! :(");
            e.printStackTrace();
        } finally {
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
    }
    
    /**
     * Remove um cliente cadastrado da base de dados com o `id` especificado.
     * @param idCliente 
     */
    public static void remover(int idCliente) {
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("DELETE FROM tb_cliente WHERE id_cliente = ?;");
            st.setInt(1, idCliente);
            st.executeUpdate();
        } catch(Exception e) {
            System.out.println("Erro ao remover cliente! :(");
        } finally {
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
    }
    
}