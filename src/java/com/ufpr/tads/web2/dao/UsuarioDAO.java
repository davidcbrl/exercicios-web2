package com.ufpr.tads.web2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.ufpr.tads.web2.beans.UsuarioBean;

public class UsuarioDAO {
    
    /**
     * Insere um usuário na base de dados.
     * @param usuario 
     */
    public static void inserir(UsuarioBean usuario) {
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("INSERT INTO tb_usuario (nome_usuario, login_usuario, senha_usuario) VALUES (?, ?, md5(?));");
            st.setString(1, usuario.getNome());
            st.setString(2, usuario.getUsuario());
            st.setString(3, usuario.getSenha());
            st.executeUpdate();
        } catch(Exception e) {
            System.out.println("Erro ao cadastrar usuário! :(");
        } finally {
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
    }
    
    /**
     * Busca todos os usuários cadastrados na base de dados.
     * @return A `lista` de usuário cadastrados.
     */
    public static List<UsuarioBean> buscarTodos() {
        List<UsuarioBean> resultados = new ArrayList<UsuarioBean>();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_usuario;");
            rs = st.executeQuery();
            while (rs.next()) {
                UsuarioBean usu = new UsuarioBean();
                usu.setId(rs.getInt("id_usuario"));
                usu.setNome(rs.getString("nome_usuario"));
                usu.setUsuario(rs.getString("login_usuario"));
                usu.setSenha(rs.getString("senha_usuario"));
                resultados.add(usu);
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter usuários! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return resultados;
    }
    
    /**
     * Busca um usuario cadastrado na base de dados com o `id` especificado.
     * @param idUsuario
     * @return O `objeto` de usuario se existir, senão retorna `null`.
     */
    public static UsuarioBean buscarPorId(int idUsuario) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_usuario WHERE id_usuario = ?;");
            st.setInt(1, idUsuario);
            rs = st.executeQuery();
            if (rs.next()) {
                UsuarioBean usu = new UsuarioBean();
                usu.setId(rs.getInt("id_usuario"));
                usu.setNome(rs.getString("nome_usuario"));
                usu.setSenha(rs.getString("senha_usuario"));
                return usu;
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter dados do usuario! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return null;
    }

    /**
     * Verifica se o usuário especificado está cadastrado na base de dados.
     * @param usuario
     * @param senha
     * @return O objeto `usuário` se existir, senão retorna `null`.
     */
    public static UsuarioBean isUsuario(String usuario, String senha) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_usuario WHERE login_usuario = ? AND senha_usuario = ?");
            st.setString(1, usuario);
            st.setString(2, senha);
            rs = st.executeQuery();
            if(rs.next()) {
                UsuarioBean usu = new UsuarioBean();
                usu.setId(rs.getInt("id_usuario"));
                usu.setNome(rs.getString("nome_usuario"));
                usu.setUsuario(rs.getString("login_usuario"));
                usu.setSenha(rs.getString("senha_usuario"));
                return usu;
            }
        } catch(Exception e) {
            System.out.println("Erro ao verificar login do usuário! :(");
        }
        finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return null;
    }
    
}