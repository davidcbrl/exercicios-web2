package com.ufpr.tads.web2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.ufpr.tads.web2.beans.CidadeBean;
import com.ufpr.tads.web2.beans.EstadoBean;

public class CidadeDAO {

    /**
     * Busca todos os cidades cadastrados na base de dados.
     * @return A `lista` de cidades cadastrados.
     */
    public static List<CidadeBean> buscarTodos(int idEstado) {
        List<CidadeBean> resultados = new ArrayList<CidadeBean>();
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_cidade WHERE id_estado = ?;");
            st.setInt(1, idEstado);
            rs = st.executeQuery();
            while (rs.next()) {
                CidadeBean cid = new CidadeBean();
                cid.setId(rs.getInt("id_cidade"));
                cid.setNome(rs.getString("nome_cidade"));
                resultados.add(cid);
            }
        } catch(Exception e) {
            System.out.println("Erro ao obter cidades! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return resultados;
    }
    
    /**
     * Busca um cidade cadastrado na base de dados com o `id` especificado.
     * @param idCidade
     * @return O `objeto` de cidade se existir, senão retorna `null`.
     */
    public static CidadeBean buscarPorId(int idCidade) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            st = conn.prepareStatement("SELECT * FROM tb_cidade WHERE id_cidade = ?;");
            st.setInt(1, idCidade);
            rs = st.executeQuery();
            if (rs.next()) {
                CidadeBean cid = new CidadeBean();
                cid.setId(rs.getInt("id_cidade"));
                cid.setNome(rs.getString("nome_cidade"));
                EstadoBean est = EstadoDAO.buscarPorId(rs.getInt("id_estado"));
                cid.setEstado(est);
                return cid;
            }                
        } catch(Exception e) {
            System.out.println("Erro ao obter dados do cidade! :(");
        } finally {
            if (rs != null) {
                try { rs.close(); } catch (Exception e) {}
            }
            if (st != null) {
                try { st.close(); } catch (Exception e) {}
            }
            if (conn != null) {
                try { conn.close(); } catch (Exception e) {}
            }
        }
        return null;
    }
    
}