package com.ufpr.tads.web2.facade;

import com.ufpr.tads.web2.beans.UsuarioBean;
import com.ufpr.tads.web2.dao.UsuarioDAO;

public class LoginFacade {
    
    public static UsuarioBean isUsuario(String usuario, String senha) {
        return UsuarioDAO.isUsuario(usuario, senha);
    }
    
}
