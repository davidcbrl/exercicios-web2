package com.ufpr.tads.web2.facade;

import java.util.List;

import com.ufpr.tads.web2.beans.AtendimentoBean;
import com.ufpr.tads.web2.beans.TipoAtendimentoBean;
import com.ufpr.tads.web2.beans.UsuarioBean;
import com.ufpr.tads.web2.beans.ClienteBean;
import com.ufpr.tads.web2.beans.ProdutoBean;
import com.ufpr.tads.web2.dao.AtendimentoDAO;
import com.ufpr.tads.web2.dao.TipoAtendimentoDAO;
import com.ufpr.tads.web2.dao.UsuarioDAO;
import com.ufpr.tads.web2.dao.ClienteDAO;
import com.ufpr.tads.web2.dao.ProdutoDAO;

public class AtendimentoFacade {
    
    public static void inserir(AtendimentoBean atd) {
        AtendimentoDAO.inserir(atd);
    } 
    
    public static AtendimentoBean buscar(int id) {
        return AtendimentoDAO.buscarPorId(id);
    }
    
    public static List<AtendimentoBean> buscarTodos() {
        return AtendimentoDAO.buscarTodos();
    }
    
    public static List<TipoAtendimentoBean> buscarTiposAtendimento() {
        return TipoAtendimentoDAO.buscarTodos();
    }
    
    public static List<UsuarioBean> buscarUsuarios() {
        return UsuarioDAO.buscarTodos();
    }

    public static List<ClienteBean> buscarClientes() {
        return ClienteDAO.buscarTodos();
    }
    
    public static List<ProdutoBean> buscarProdutos() {
        return ProdutoDAO.buscarTodos();
    }
    
    public static TipoAtendimentoBean buscarTipo(int id) {
        return TipoAtendimentoDAO.buscarPorId(id);
    }
    
    public static UsuarioBean buscarUsuario(int id) {
        return UsuarioDAO.buscarPorId(id);
    }
    
    public static ClienteBean buscarCliente(int id) {
        return ClienteDAO.buscarPorId(id);
    }
    
    public static ProdutoBean buscarProduto(int id) {
        return ProdutoDAO.buscarPorId(id);
    }
    
}
