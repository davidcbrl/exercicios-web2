package com.ufpr.tads.web2.facade;

import java.util.List;

import com.ufpr.tads.web2.beans.UsuarioBean;
import com.ufpr.tads.web2.dao.UsuarioDAO;

public class UsuarioFacade {
    
    public static void inserir(UsuarioBean usu) {
        UsuarioDAO.inserir(usu);
    }
    
    public static UsuarioBean buscar(int id) {
        return UsuarioDAO.buscarPorId(id);
    }
    
    public static List<UsuarioBean> buscarTodos() {
        return UsuarioDAO.buscarTodos();
    }
    
}
