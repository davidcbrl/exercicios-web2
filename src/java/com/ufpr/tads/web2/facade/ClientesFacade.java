package com.ufpr.tads.web2.facade;

import java.util.List;

import com.ufpr.tads.web2.beans.ClienteBean;
import com.ufpr.tads.web2.beans.EstadoBean;
import com.ufpr.tads.web2.beans.CidadeBean;
import com.ufpr.tads.web2.dao.CidadeDAO;
import com.ufpr.tads.web2.dao.ClienteDAO;
import com.ufpr.tads.web2.dao.EstadoDAO;

public class ClientesFacade {
    
    public static void inserir(ClienteBean cli) {
        ClienteDAO.inserir(cli);
    }
        
    public static void alterar(ClienteBean cli) {
        ClienteDAO.alterar(cli);
    }
    
    public static void remover(int id) {
        ClienteDAO.remover(id);
    }
    
    public static ClienteBean buscar(int id) {
        return ClienteDAO.buscarPorId(id);
    }
    
    public static List<ClienteBean> buscarTodos() {
        return ClienteDAO.buscarTodos();
    }
    
    public static List<EstadoBean> buscarEstados() {
        return EstadoDAO.buscarTodos();
    }

    public static List<CidadeBean> buscarCidades(int id) {
        return CidadeDAO.buscarTodos(id);
    }
    
    public static CidadeBean buscarCidadeCliente(int id) {
        return CidadeDAO.buscarPorId(id);
    }
    
}
