package com.ufpr.tads.web2.beans;

import java.io.Serializable;

/**
 * Configuração global da aplicação.
 */
public class ConfigBean implements Serializable {
    
    private String email;
    
    public ConfigBean() {
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
}
