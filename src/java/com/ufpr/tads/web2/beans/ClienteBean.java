package com.ufpr.tads.web2.beans;

import java.io.Serializable;
import java.util.Date;

public class ClienteBean implements Serializable {
    
    private int id;
    private String cpf;
    private String nome;
    private String email;
    private Date data;
    private String rua;
    private int numero;
    private String cep;
    private CidadeBean cidade;
    
    public ClienteBean() {
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public Date getData() {
        return data;
    }
    public void setData(Date data) {
        this.data = data;
    }

    public String getRua() {
        return rua;
    }
    public void setRua(String rua) {
        this.rua = rua;
    }

    public int getNumero() {
        return numero;
    }
    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCep() {
        return cep;
    }
    public void setCep(String cep) {
        this.cep = cep;
    }

    public CidadeBean getCidade() {
        return cidade;
    }
    public void setCidade(CidadeBean cidade) {
        this.cidade = cidade;
    }

}
