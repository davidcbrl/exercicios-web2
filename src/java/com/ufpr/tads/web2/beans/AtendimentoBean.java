package com.ufpr.tads.web2.beans;

import java.io.Serializable;
import java.util.Date;

public class AtendimentoBean implements Serializable {
    
    private int id;
    private String descricao;
    private TipoAtendimentoBean tipo;
    private Date data;
    private String resolvido;
    private UsuarioBean usuario;
    private ClienteBean cliente;
    private ProdutoBean produto;

    public AtendimentoBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoAtendimentoBean getTipo() {
        return tipo;
    }

    public void setTipo(TipoAtendimentoBean tipo) {
        this.tipo = tipo;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getResolvido() {
        return resolvido;
    }

    public void setResolvido(String resolvido) {
        this.resolvido = resolvido;
    }

    public UsuarioBean getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioBean usuario) {
        this.usuario = usuario;
    }

    public ClienteBean getCliente() {
        return cliente;
    }

    public void setCliente(ClienteBean cliente) {
        this.cliente = cliente;
    }

    public ProdutoBean getProduto() {
        return produto;
    }

    public void setProduto(ProdutoBean produto) {
        this.produto = produto;
    }
    
}
